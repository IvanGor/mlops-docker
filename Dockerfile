FROM python:3.9-slim
WORKDIR /app
# Install git and pdm
RUN apt-get update && apt-get install -y git && pip install pdm
COPY .pre-commit-config.yaml pyproject.toml pdm.lock* /app/
RUN git init \
    && git config user.name "Your name" \
    && git config user.email "your@email.com" \
    && git add . \
    && git commit -m "Initial commit"
RUN pdm install

RUN pdm add --dev pre-commit
RUN pdm add --dev ruff
RUN pdm run pre-commit install
