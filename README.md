# mlops_project_docker
Docker container for mlops course with pre-commit and ruff.
To build and run:
1. After clonning the repository, go into the folder with dockerfile.
2. Run "docker build -t my-mlops-app ."
3. Run "docker run -it -d my-mlops-app"
4. Check if the container is up and running: "docker ps -all"